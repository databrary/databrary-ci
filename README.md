# Databrary [<img src="https://img.shields.io/badge/coverage-15%25-yellow.svg">](http://databrary.github.io/databrary/coverage/hpc_index.html) [<img src="https://img.shields.io/badge/haddocks-generated-brightgreen.svg">](http://databrary.github.io/databrary/haddocks/) [<img src="https://img.shields.io/badge/jsdoc-generated-brightgreen.svg">](http://databrary.github.io/databrary/frontend-doc/) [<img src="https://img.shields.io/uptimerobot/ratio/m779481982-e616968da86ff263aea86978.svg">](https://stats.uptimerobot.com/J80YrsnzP)

Want to help out? Feel free to start right here with this underwhelming README!

We have [preliminary library
documentation](http://databrary.github.io/databrary/) on our GitHub Pages site.
Haddocks coverage would be another excellent place to start contributing...

A simple [code coverage report](http://databrary.github.io/databrary/) is also
available on our GitHub pages site.

## License

Copyright (C) 2013-2018 New York University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
